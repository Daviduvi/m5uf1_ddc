import java.util.Scanner;

public class Main {

    private Scanner sc = new Scanner(System.in);

    public void showMenu(){
        System.out.println("Escoge una opcion:");
        System.out.println("1.Suma d'Arrays");
        System.out.println("0. Multiplicació d'Arrays\n");
    }

    public int readOpcion() {
        return sc.nextInt();
    }

    public void operar(int opcion){

        switch (opcion){
            case 0:
                mulArray_ddc();
                break;
            case 1: addArray_ddc();
                break;
        }
    }

    /* OPERATIONS */
    public void mulArray_ddc(){
        System.out.println("Introdueix el nombre d'elements que tindrà l'array: ");
        int llargada = sc.nextInt();

        int array1 [] = new int [llargada];
        int array2 [] = new int [llargada];
        int mult [] = new int [llargada];

        System.out.println("Introdueix els nombres a multiplicar: ");
        for(int i = 0; i < llargada; i++){
            array1[i] = sc.nextInt();
            array2[i] = sc.nextInt();
            mult[i] = array1[i] * array2[i];
            System.out.println(array1[i]+" * "+array2[i]+" = "+mult[i]);
        }
    }
    public void addArray_ddc(){
        System.out.println("Introdueix el nombre d'elements que tindrà l'array: ");
        int llargada = sc.nextInt();

        int array1 [] = new int [llargada];
        int array2 [] = new int [llargada];
        int suma [] = new int [llargada];

        System.out.println("Introdueix els nombres a sumar: ");
        for(int i = 0; i < llargada; i++){
            array1[i] = sc.nextInt();
            array2[i] = sc.nextInt();
            suma[i] = array1[i] + array2[i];
            System.out.println(array1[i]+" + "+array2[i]+" = "+suma[i]);
        }
    }


    public static void main(String[] args) {

        Main Main = new Main();

        int response = 0;
        do{
            Main.showMenu();
            response = Main.readOpcion();
            Main.operar(response);
        }while(response != 0);

    }
}

